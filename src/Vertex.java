package graph;

import java.util.ArrayList;
import java.util.List;

public class Vertex
{

  private boolean visited;
  private String key;
  ArrayList<Edge> edges;

  public Vertex(String name)
  {
    key = name;
    visited = false;
    edges = new ArrayList<Edge>();
  }

  public String getKey()
  {
    return key;
  }

  public boolean hasBeenVisited()
  {
    return visited;
  }

  public ArrayList<Edge> getEdges()
  {
    return edges;
  }

  public void addEdge(Edge edge)
  {
    edges.add(edge);
  }

  public void unVisit()
  {
    visited = false;
  }

  public void visited()
  {
    visited = true;
  }

  public int numEdges()
  {
    return edges.size();
  }

  public Edge getEdge(int index)
  {
    return edges.get(index);
  }

}
