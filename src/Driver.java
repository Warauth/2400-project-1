package graph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Driver
{
  public static void main(String [] args)
  {
    String current_line;

    ArrayList<Vertex> verts = new ArrayList<Vertex>();

    ArrayList<String> edge_start_strings = new ArrayList<String>();
    ArrayList<String> edge_end_strings = new ArrayList<String>();
    ArrayList<Double> edge_weights = new ArrayList<Double>();

    int count = 0;

    Graph roman_graph;

    //This try block creates the BufferedReaders I will be using to read in the data from the text files.
    try
    {

      BufferedReader br_vertices = new BufferedReader(new FileReader(args[0]));
      BufferedReader br_edges = new BufferedReader(new FileReader(args[1]));

      while ((current_line = br_vertices.readLine()) != null)
      {
        Vertex vert = new Vertex(current_line);
        verts.add(vert);
        count++;
			}

      while ((current_line = br_edges.readLine()) != null)
      {
        int start_index = current_line.indexOf(" ", 0);
        String edge_start = current_line.substring(0, start_index);
        edge_start_strings.add(edge_start);

        int end_index = current_line.indexOf(" ", start_index + 1);
        String edge_end = current_line.substring(start_index + 1, end_index);
        edge_end_strings.add(edge_end);

        String weight_string = current_line.substring(end_index + 1);
        double weight = Double.parseDouble(weight_string);
        edge_weights.add(weight);
      }

    }

    //If for some reason, the files are not there, the program will catch it and terminate.
    catch (IOException e)
    {
      e.printStackTrace();
    }

    roman_graph = new Graph(count);

    for (int i = 0; i < count; i++)
    {
      roman_graph.addVertex(verts.get(i));
    }

    //adding the edges to the adjacency matrix and the proper vertices
    for (int i = 0; i < edge_start_strings.size(); i++)
    {
      roman_graph.addEdge(edge_start_strings.get(i), edge_end_strings.get(i), edge_weights.get(i));
    }

    //computing the depth first search
    ArrayList<Vertex> depth_first_search = roman_graph.dfs();

    System.out.println("DFS:\n");

    //printing out the depth first search
    System.out.println("dfs size: " + depth_first_search.size() + "\n");
    for (int i = 0; i < depth_first_search.size(); i++)
    {
      System.out.println(depth_first_search.get(i).getKey());
    }

    System.out.println("\n");

    ArrayList<Vertex> topSort = roman_graph.topological_sort();

    System.out.println("Topological sort: \n");

    for (int i = 0; i < topSort.size(); i++)
    {
      System.out.println(topSort.get(i).getKey());
    }

    System.out.println("\nBFS:\n");

    //computing the breadth first search
    ArrayList<Vertex> breadth_first_search = roman_graph.bfs();

    //printing out the depth first search
    for (int i = 0; i < breadth_first_search.size(); i++)
    {
      System.out.println(breadth_first_search.get(i).getKey());
    }

  }
}
