package graph;

public class Edge
{

  private String start_key;
  private String end_key;
  private double weight;

  public Edge(String start_vertex_key, String end_vertex_key, double edge_weight)
  {
    start_key = start_vertex_key;
    end_key = end_vertex_key;
    weight = edge_weight;
  }

  //Returns the "Start key" for the edge (the name of the vertex from which the edge originates).
  public String getStartKey()
  {
    return start_key;
  }

  //Returns the "End key" for the edge (the name of the vertex connected to the start vertex by the edge).
  public String getEndKey()
  {
    return end_key;
  }

  //Returns the weight (in the form of a double) of the edge.
  public double getWeight()
  {
    return weight;
  }

}
