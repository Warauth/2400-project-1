package graph;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

public class Graph
{

  private int max_vertices;
  private int num_vertices;
  private ArrayList<Vertex> vertices;
  private double [][] adj_matrix;

  //constructor accepts the maximum number of vertices allowed in the graph
  public Graph(int max_num_vertices)
  {
    max_vertices = max_num_vertices;
    num_vertices = 0;
    vertices = new ArrayList<Vertex>(max_num_vertices);
    adj_matrix = new double[max_vertices][max_vertices];
  }

  //allow items (items are identified by String search keys) to be stored in the Graph vertices
  public void addVertex(Vertex item)
  {
    for (int i = 0; i < num_vertices; i++)
    {
      //if the vertices are the same, ignore them
      if (vertices.get(i).getKey().equals(item.getKey()))
      {
        System.out.println("Cannot add duplicate vertices.\n");
        return;
      }
    }
    //add the vertices to the list
    vertices.add(item);
    num_vertices++;
  }


  //add a directed edge between two vertices
  public void addEdge(String start_vertex_key, String end_vertex_key, double edge_weight)
  {
    //if it is trying to add an edge to itself, ignores.
    if (start_vertex_key.equals(end_vertex_key))
    {
      System.out.println("Cannot add an edge to the same vertex.\n");
      return;
    }

    //if the edge has no weight, ignores
    if (edge_weight == 0.0)
    {
      System.out.println("Cannot add an edge that has no weight.\n");
      return;
    }

    //computes the indices of the starting vertex and the ending vertex in the list.
    int start_index = getVertexIndex(start_vertex_key);
    int end_index = getVertexIndex(end_vertex_key);

    //if either the start index or end index is -1 (not found), ignores
    if (start_index == -1 || end_index == -1)
    {
      System.out.println("One or both of the edges do not exist.\n");
      return;
    }

    //sets the weight in the proper slot of the adjacency matrix
    adj_matrix [start_index][end_index] = edge_weight;

    //creates the edge to put in the proper (starting) vertex
    Edge edge_to_add = new Edge(start_vertex_key, end_vertex_key, edge_weight);
    Vertex vert = vertices.get(start_index);

    //adds the edge to the list of edges that the vertex contains
    vert.addEdge(edge_to_add);
  }

  //perform a depth first search, adding items to a linked list
  public ArrayList<Vertex> dfs()
  {



    //creates the list that will store the results of the depth first search
    ArrayList<Vertex> depth_list = new ArrayList<Vertex>();

    //looping through the vertices, looking for one that has not yet been visited
    for (int i = 0; i < num_vertices; i++)
    {
      Vertex current_vertex = vertices.get(i);

      //if it hasnt been visited, enters into the recursive function for the dfs
      if (!(current_vertex.hasBeenVisited()))
      {
        dfs_rec(current_vertex, depth_list);
      }

    }

    //after the dfs has been completed, set the vertices back to being unvisisted;
    makeAllVerticesUnvisited();

    return depth_list;

  }

  private void dfs_rec(Vertex vert, ArrayList<Vertex> depth_list)
  {
    //set the current vertex to be visited
    vert.visited();

    //add the vertex to the solution list
    depth_list.add(vert);

    //find the vertex in the list of all vertices, and get its index
    int row_index = vertices.indexOf(vert);

    //loop through the adjacent vertices in the adjacency matrix, looking for one that has a valid weight
    for (int i = 0; i < num_vertices; i++)
    {
      //if an adjacent matrix is found, checks if it has been visited
      if (adj_matrix[row_index][i] > 0.0)
      {
        Vertex next_vert = vertices.get(i);

        //if it hasnt, visit it, and enter another recursive call.
        if (!(next_vert.hasBeenVisited()))
        {
          dfs_rec(next_vert, depth_list);
        }
      }
    }

  }

  //perform a breadth first search, adding items to a linked list
  public ArrayList<Vertex> bfs()
  {
    //creating the solution list
    ArrayList<Vertex> breadth_list = new ArrayList<Vertex>();

    //finding the first unvisited vertex
    for (int i = 0; i < num_vertices; i++)
    {
      Vertex current_vertex = vertices.get(i);

      //if it hasnt been visited, enter the recursive call and visit it
      if (!(current_vertex.hasBeenVisited()))
      {
        //recursive call
        bfs_rec(current_vertex, breadth_list);
      }

    }

    //after the dfs is done, set all vertices to unvisited
    makeAllVerticesUnvisited();

    return breadth_list;
  }

  public void bfs_rec(Vertex vert, ArrayList<Vertex> breadth_list)
  {
    //creating the queue for the bfs
    LinkedList<Vertex> queue = new LinkedList<Vertex>();

    //set the current vertex to be visited
    vert.visited();

    //add it to the queue
    queue.add(vert);

    //and add it to the solution list
    breadth_list.add(vert);

    //while the queue isnt null, keep looking for another vertex
    while (queue.peek() != null)
    {
      //find the next vertex in the queue
      Vertex current_vert = queue.peek();

      //loop through the edges in the current vertex
      for (int i = 0; i < current_vert.numEdges(); i++)
      {
        //get its end key
        String end_key = current_vert.getEdge(i).getEndKey();

        //get the index of the vertex with the corresponding end key
        int next_vert_index = getVertexIndex(end_key);

        //get the vertex based on the index we just found
        Vertex next_vert = vertices.get(next_vert_index);

        //if it hasnt been visted
        if (!(next_vert.hasBeenVisited()))
        {
          //visit it
          next_vert.visited();

          //add it to the solution list,
          breadth_list.add(next_vert);

          //and to the queue
          queue.add(next_vert);
        }
      }

      //when an unvisited node cant be found, pop from the queue
      queue.remove();

    }

  }

  public ArrayList<Vertex> topological_sort()
  {
    ArrayList<Vertex> dfsList = dfs();

    ArrayList<Vertex> topSortList = new ArrayList<Vertex>(dfsList.size());

    int index = dfsList.size() - 1;

    for (int i = 0; i < dfsList.size(); i++)
    {
      topSortList.add(dfsList.get(index));
      index--;
    }

    return topSortList;
  }

  //find the index of the vertex in the vertices list via the key
  private int getVertexIndex(String key)
  {

    for (int i = 0; i < num_vertices; i++)
    {
      if (vertices.get(i).getKey().equals(key))
      {
        return i;
      }
    }

    return -1;
  }

  //set all vertices to be visited
  private void makeAllVerticesUnvisited()
  {
    for (int i = 0; i < num_vertices; i++)
    {
      vertices.get(i).unVisit();
    }
  }

  //displays all the vertices in the vertices list
  public void displayAllVertices()
  {
    System.out.println("\nVERTICES ARE:");
    for (int i = 0; i < num_vertices; i++)
    {
      System.out.println(vertices.get(i).getKey());
    }
  }

  private double [][] createAdjMatrix()
  {
    double [][] matrix = new double[max_vertices][max_vertices];

    for (int i = 0; i < vertices.size(); i++)
    {
      for (int j = 0; j < vertices.size(); j++)
      {
        matrix[i][j] = adj_matrix[i][j];
        matrix[j][i] = matrix[i][j];
      }
    }

    return matrix;
  }

}
