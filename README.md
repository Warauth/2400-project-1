This README file is for Program 1 of David Brown's CSC2400 Design of Algorithms class.  

Needed:

- Topological Sort

How to run with passed-in arguments on terminal:  
```
$ ant run -Darg1='example1.txt' -Darg2='example2.txt'
```
